﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class MainMenuUIController : MonoBehaviour
{
    [SerializeField] private Button m_playButton;
    [SerializeField] private Button m_quitButton;
    [SerializeField] private Animator m_mainCamAnim;
    [SerializeField] private AnimationClip[] animations;
    public GameObject GameUI;

    CompositeDisposable disposables = new CompositeDisposable();


    private void Awake()
    {
        AddButtonListeners();  
        StartCoroutine(StartOnAwake());
    }
    private void Start()
    {
        LoadingScreenManager.Instance.LoadingScreen();
    }
    private IEnumerator StartOnAwake()
    {
        m_mainCamAnim.SetTrigger("Awake");  
        
        yield return new WaitForSeconds(m_mainCamAnim.GetCurrentAnimatorStateInfo(0).length+m_mainCamAnim.GetCurrentAnimatorStateInfo(0).normalizedTime);

        GameUI.SetActive(true);
    }

    private void OnDisable()
    {
        disposables.Clear();
    }

    private void AddButtonListeners()
    {
        m_playButton.OnClickAsObservable()
            .Subscribe(x => OnClickPlayButton())
            .AddTo(disposables);

        m_quitButton.OnClickAsObservable()
            .Subscribe(x => 
            {
                Application.Quit();
            });
    }

    private void OnClickPlayButton()
    {
        StartCoroutine(StartOnClick());
    }
    
    public IEnumerator StartOnClick()
    {
        GameUI.SetActive(false);
        m_mainCamAnim.SetTrigger("Play");
        
        yield return new WaitForSeconds(m_mainCamAnim.GetCurrentAnimatorStateInfo(0).length+m_mainCamAnim.GetCurrentAnimatorStateInfo(0).normalizedTime);
        LoadingScreenManager.Instance.LoadingScreen();
        yield return new WaitForSeconds(2);
        UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
        // UnityEngine.SceneManagement.SceneManager.LoadScene("TestWisp");
    }

}
