﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class GameSceneUIController : MonoBehaviour
{
    [SerializeField] private Button m_homeButton;
    [SerializeField] private CompositeDisposable disposables = new CompositeDisposable();
    private void Awake()
    {
        AddButtonListeners();
        LoadingScreenManager.Instance.LoadingScreen();
    }

    private void OnDestroy()
    {
        disposables.Clear();
    }

    private void AddButtonListeners()
    {
        m_homeButton.OnClickAsObservable()
            .Subscribe(x => 
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            });
    }
}
