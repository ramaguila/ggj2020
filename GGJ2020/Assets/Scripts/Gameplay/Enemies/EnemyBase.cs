﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBase : MonoBehaviour
{
    public Rigidbody m_rigidBody;
    public int m_moveSpeed;
   public bool  m_canMove;
    public float m_distanceToFollow;
    public bool m_isFlyingEnemy = false;

    public  GameObject m_bulletPrefab;

    float shootingTimer;
    public float shootingRate;

    public virtual void Awake()
    {
       m_rigidBody = this.GetComponent<Rigidbody>();
     
    }

    public virtual void OnBecameVisible()
    {
        m_canMove = true;
    }

    public void FixedUpdate()
    {
        CheckDistance();

        if (m_canMove)
        {
          
            // m_rigidBody.MovePosition(PlayerController.Instance.transform.position * m_moveSpeed);
            transform.position = Vector3.MoveTowards(this.gameObject.transform.position, PlayerController.Instance.transform.position, m_moveSpeed * Time.deltaTime);
            if (m_isFlyingEnemy)
            {
                m_rigidBody.velocity = Vector3.zero;
            }
            ShootBullet();
        }
        else
        {
            CancelInvoke();
        }
    }

    void CheckDistance()
    {
        float distance = Vector3.Distance(PlayerController.Instance.transform.position, transform.position);
        Debug.Log("Distance =  " + distance);

        if(distance <= m_distanceToFollow)
        {
            m_canMove = true;
        }
        else
        {
            m_canMove = false;
        }
  
      
      
    }

    void ShootBullet()
    {
         shootingTimer += Time.deltaTime;

        if (shootingTimer >= shootingRate)
        {
            GameObject bullet = GameObject.Instantiate(this.m_bulletPrefab, transform.position, Quaternion.identity) as GameObject;
            if (PlayerController.Instance.transform.position.x > transform.position.x)
            {
                //Right side.
                bullet.GetComponent<BulletController>().Init(1);
                bullet.transform.position += Vector3.right * 2;

            }
            else
            {
                //Left side.
                bullet.transform.position -= Vector3.right * 2;

                bullet.transform.rotation = new Quaternion(0, 0, -180, 0);
                bullet.GetComponent<BulletController>().Init(1);
            }
            shootingTimer = 0;
        }
    }
}
