﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public Transform playerTransform;
    [SerializeField] private Rigidbody m_rigidBody;
    [SerializeField] private int m_moveSpeed = 5;
    [SerializeField] private int m_jumpForce = 7;
    [SerializeField] private LayerMask m_LayerMask;
    [SerializeField] private GameObject m_groundChecker;
    [SerializeField] private GameObject m_bulletPrefab;
    [SerializeField] private Collider[] hitColliders;
    [SerializeField] private OrbitController[] m_orbitController;
    private bool TriggerResetBullets = false;

    #region temporary floats
    [SerializeField] private float m_maxHeight;
    [SerializeField] private float m_minHeight;
    [SerializeField] private float m_currentHeight;
    [SerializeField] private float m_jumpPosition;
    #endregion

    #region Movement Bool
    [SerializeField] private bool m_isGrounded = true;
    [SerializeField] private bool m_isGliding = false;
    [SerializeField] private bool m_isFlying = false;
    #endregion 

    [SerializeField] private int m_bulletCounter;

    private void Awake()
    {
        Instance = this;
        m_bulletCounter = 3;
    }

    private void Update()
    {
        if (m_isFlying)
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                m_rigidBody.velocity = new Vector2(Input.GetAxis("Horizontal") * m_moveSpeed, Input.GetAxis("Vertical") * m_moveSpeed);
            }
        }

        else
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                m_rigidBody.velocity = new Vector2(Input.GetAxis("Horizontal") * m_moveSpeed, m_rigidBody.velocity.y);
            }
        }



        if (Input.GetKeyDown(KeyCode.Space) && m_isGrounded)
        {

            // m_rigidBody.AddForceAtPosition(Vector3.up, transform.position, ForceMode.Impulse);
            m_maxHeight = m_rigidBody.transform.position.y;
            m_jumpPosition = m_rigidBody.transform.position.y;

            m_maxHeight = Rounder(m_maxHeight);
            m_jumpPosition = Rounder(m_jumpPosition);

            m_rigidBody.velocity = new Vector2(m_rigidBody.velocity.x, m_jumpForce);
            m_isGrounded = false;
            hitColliders = null;
        }


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            m_isFlying = true;
            m_isGliding = false;
            m_isGrounded = false;
        }

        // Check if player jumped
        if (!m_isGrounded && !m_isGliding && !m_isFlying)
        {
            m_currentHeight = m_rigidBody.transform.position.y;
            m_currentHeight = Rounder(m_currentHeight);
            m_maxHeight = Rounder(m_maxHeight);
            if (m_currentHeight > m_maxHeight)
            {
                m_maxHeight = m_currentHeight;
            }
            else if (m_currentHeight < m_maxHeight)
            {
                m_minHeight = m_rigidBody.transform.position.y;
                m_minHeight = Rounder(m_minHeight);
                m_isGliding = true;
            }
        }


        if (Input.GetKeyDown(KeyCode.P))
        {
            for (int i = 0; i < m_orbitController.Length; i++)
            {
                if (!m_orbitController[i].IsEmitting())
                {
                    m_orbitController[i].PlayParticle();
                    return;
                }
            }

        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            for (int i = 0; i < m_orbitController.Length; i++)
            {
                if (m_orbitController[i].IsEmitting())
                {
                    m_orbitController[i].StopParticle();
                    return;
                }
            }

        }

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            if(m_bulletCounter > 0 && !TriggerResetBullets)
            {
                GameObject bullet = GameObject.Instantiate(this.m_bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                m_bulletCounter--;
                m_orbitController[m_bulletCounter].StopParticle();
                if(Input.mousePosition.x > Screen.width * 0.5f)
                {
                    //Right side.
                    bullet.GetComponent<BulletController>().Init(1);
                    
                }
                else
                {
                    //Left side.
                    bullet.transform.rotation = new Quaternion (0,0,-180,0);
                    bullet.GetComponent<BulletController>().Init(1);
                }
                

                if(m_bulletCounter == 0)
                {
                    TriggerResetBullets = true;
                    Invoke("ResetBullets", 1);
                }
            }
              
        }
    }

    private void FixedUpdate()
    {   
        if(m_isGliding || m_isFlying)
            GroundedCheck();

        
        
    }

    private void ResetBullets()
    {
        m_bulletCounter = 3;
        TriggerResetBullets = false;
        foreach(OrbitController orbit in m_orbitController)
        {
            if (!orbit.IsEmitting())
            {
                orbit.PlayParticle();
            }
        }

    }
    private void GroundedCheck()
    {
        
        hitColliders = Physics.OverlapBox(gameObject.transform.position, transform.localScale / 2, Quaternion.identity, m_LayerMask);
        if(hitColliders.Length > 0)
        {
            m_isGrounded = true;
            m_isGliding = false;
            m_isFlying = false;
        }
    }
    private float Rounder(float p_toRound)
    {
        return Mathf.Round(p_toRound * 100f) / 100f;
    }


}