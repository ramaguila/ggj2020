﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public static Explosion Instance;
    [SerializeField] private GameObject m_explosionArea;
    [SerializeField] private float m_explosionPower = 10.0f;
    [SerializeField] private float m_explosionRadius = 5.0f;
    [SerializeField] private float m_upForce = 1.0f;

    private void Awake()
    {
        Instance = this;
        Invoke("Detonate", 3);
    }
    
    public void Detonate()
    {
        Debug.Log("Kaboom");
        Vector3 explosionPosition = m_explosionArea.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, m_explosionRadius);

        foreach(Collider hit in colliders)
        {
            Debug.Log(hit.name);
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if(rb != null)
            {
                Debug.Log("Rigidbody Detected");
                rb.useGravity = true;
                rb.AddExplosionForce(m_explosionPower, explosionPosition, m_explosionRadius, m_upForce, ForceMode.Impulse);
            }
        }
    }
}
