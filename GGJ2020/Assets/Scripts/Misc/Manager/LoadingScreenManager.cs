﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenManager : MonoBehaviour
{
    public static LoadingScreenManager Instance;
    [SerializeField] private GameObject m_loadingScreen;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }


    public void LoadingScreen()
    {
        if(m_loadingScreen.activeSelf)
        {
            m_loadingScreen.SetActive(false);
        }
        else
        {
            m_loadingScreen.SetActive(true);
        }
    }
}
