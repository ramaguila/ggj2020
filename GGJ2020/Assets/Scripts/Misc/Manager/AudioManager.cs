﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] private AudioSource m_ambientSource;
    [SerializeField] private AudioSource m_ambient2Source;
    [SerializeField] private AudioSource m_oneShotSource;

    private AudioClip m_lobbyAudio;

    [SerializeField] private AudioClip[] m_inGameAudio;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void Init()
    {
        m_ambientSource.Play();
    }

    public void PlaySound(int p_indexValue)
    {
        m_oneShotSource.Pause();
        m_oneShotSource.clip = m_inGameAudio[p_indexValue];
        m_oneShotSource.UnPause();
    }

    public void PlayAmbient(int p_indexValue)
    {
        m_ambientSource.Pause();
        m_ambientSource.clip = m_inGameAudio[p_indexValue];
        m_ambientSource.UnPause();
    }

    public void PlayAmbient2(int p_indexValue)
    {
        m_ambient2Source.Pause();
        m_ambient2Source.clip = m_inGameAudio[p_indexValue];
        m_ambient2Source.UnPause();
    }
}
