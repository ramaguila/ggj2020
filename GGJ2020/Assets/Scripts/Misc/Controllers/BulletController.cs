﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    Vector3 vector;

    public void Init(int p_value)
    {
        if(p_value == 0)
        {
            vector = Vector3.left;
            Debug.Log("left");
        }
        else if(p_value == 1)
        {
            vector = Vector3.right;
            Debug.Log("right");
        }
        Destroy(this.gameObject, 8);
    }

    private void OnDestroy()
    {
        vector = Vector3.zero;
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(vector * 10 * Time.smoothDeltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        OnDestroy();
    }
}
