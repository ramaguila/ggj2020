﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitController : MonoBehaviour
{
    [SerializeField] Animator m_Animator;
    [SerializeField] float m_animSpeed;
    [SerializeField] float m_delay;

    [SerializeField] ParticleSystem m_orb;
    [SerializeField] ParticleSystem m_trail;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator.speed = m_animSpeed;
        StartCoroutine(StartAnimation());
       
    }

    IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(m_delay);
        m_Animator.SetTrigger("Play");
        PlayParticle();
    }

    public void PlayParticle()
    {
        m_orb.Play();
        m_trail.Play();
    }

    public void StopParticle()
    {
        m_orb.Clear();
        m_trail.Clear();
        m_orb.Stop();
        m_trail.Stop();

    }

    public bool IsEmitting()
    {
        if (m_orb.isEmitting)
        
            return true;

        return false;
    }



   



}
