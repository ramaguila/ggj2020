﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(InitManagers());
    }
    
    private IEnumerator InitManagers()
    {
        // Init all the managers here

        AudioManager.Instance.Init();
        // is finished? then load main menu
        LoadingScreenManager.Instance.LoadingScreen();
        yield return new WaitForSeconds(2);
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
